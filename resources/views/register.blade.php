<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        form{
            text-align: justify;
            width: 400px;
        }
        label{
            width: 40%;
            text-align: left;
        }
        input{
            width: 60%;
        }
        body{
            vertical-align: 100vh;
            text-align: center;
        }
    </style>
</head>
<body>
    <div>
        <form action="{{url('create')}}" method="POST">
            @csrf            
            <label for="name">Nama :</label><input name="nama" type="text" id="name"><br>
            <label for="color">Warna Kesukaan :</label><input name="color" type="color" id="color"><br>
            <label for="nim">NIM :</label><input name="nim" type="text" id="nim"><br>
            <button type="submit">submit</button>
        </form>
    </div>
</body>
</html>