<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $fillable = ['book_name','book_color','book_nim','id','owner_id'];

    public function owner()
    {
    	return $this->belongsTo('App\Owner');
    }
}
