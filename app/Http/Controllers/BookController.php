<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;

class BookController extends Controller
{
    public function SeeOwner()
    {
    	$books = Book::with('owner')->get();
    	return view('book',compact('books'));
    }
}
