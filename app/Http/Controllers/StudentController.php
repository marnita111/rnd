<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;
use Illuminate\Support\Facades\Auth;
use App\User;

class StudentController extends Controller
{
	public function __Construct() //buat cek user udh login apa belom
	{
		$this->middleware('auth');
	}
	
    public function CreateStudent(Request $req)
	{
		if(Auth::id!=null) {	
			$create = new Book;
			$create->book_name = $req->nama;
			$create->book_color = $req->color;
			$create->book_nim = $req->nim;
			$create->save();
			return redirect('/');
		} else {
			return redirect('/home');
		}
	}
	public function ReadStudent(Request $req)
	{
		//if(Auth::check()) {
			$read = Book::All();
			return view('read',compact('read')	);
		//} else {
			//dd("Belum login?"); //dd = ngeprint versi debug
		//}
	}
	public function DeleteStudent($id)
	{
		$delete = Book::where('id',$id)->first();
		$delete->delete();
	}
	public function UpdateStudent($id)
	{
		$update = Book::where('id',$id)->first();
		$update->book_name = "LnT";
		$update->save();
		return redirect('/read');
	}
	public function getUser()
	{
		$user = Auth::User();
		// $id = User::where('id',$user);
		// dd($id);

		return $user;
	}


}

