<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Owner extends Model
{
    protected $fillable = ['owner_name','owner_email','id'];

    public function owner()
    {
    	return $this->hasMany('App\Book');
    }
}
