<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::middleware('auth')->group(function()
{
	Route::get('/', function () {
	    return view('register');
	});

	Route::post('/create','StudentController@CreateStudent');

	Route::get('/read','StudentController@ReadStudent')->middleware('auth');

	Route::get('/del/{id}','StudentController@DeleteStudent');

	Route::get('/update/{id}','StudentController@UpdateStudent');

	Route::get('/ow','BookController@SeeOwner');

	Route::get('/user','StudentController@getUser');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

